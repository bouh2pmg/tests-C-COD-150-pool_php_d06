<?php

include_once($argv[1] . "/AssaultTerminator.php");
include_once($argv[1] . "/TacticalMarine.php");
include_once($argv[1] . "/RadScorpion.php");
include_once($argv[1] . "/SuperMutant.php");


$joe = new TacticalMarine("Joe");
$baracus = new AssaultTerminator("Baracus");

$sm = new SuperMutant();
$sm2 = new SuperMutant();

$rs = new RadScorpion();
$rs2 = new RadScorpion();
$rs3 = new RadScorpion();

$rs->moveCloseTo($joe);
$rs->attack($joe);
echo "Joe hp after being attacked by a RadScorpion : " . $joe->getHp() . "\n";
$sm->moveCloseTo($baracus);
$sm->attack($baracus);
$joe->attack($sm);
$sm->recoverAp();
echo "SuperMutant AP and HP after a recover call : " . $sm->getAp() . " && " . $sm->getHp() . "\n";