<?php

include_once($argv[1] . "/AMonster.php");
include_once($argv[1] . "/ASpaceMarine.php");

class Monster extends AMonster
{
    public function __construct($name)
    {
        parent::__construct($name);
        $this->ap =  50;
        $this->apcost = 10;
        $this->damage = 50;
        $this->hp = 1;
    }

    public function __destruct()
    {
    }
}

class SpaceMarine extends ASpaceMarine
{
    public function __construct($name)
    {
        parent::__construct($name);
    }
}

$monster = new Monster("BigFoot");
$monster->equip("");
echo "Trying to attack itself : ";
$monster->attack($monster);
echo "\n";
$m2 = new Monster("LittleFoot");
$monster->attack($m2);
echo "Trying to move close to oneself : ";
$monster->moveCloseTo($monster);
echo "\n";
$monster->moveCloseTo($m2);
$monster->moveCloseTo($m2);
$monster->attack($m2);
$m2->moveCloseTo($monster);
$m2->attack($monster);
$m2->equip("");
$m2->receiveDamage(500);

$joe = new SpaceMarine("Joe");

echo "Testing Getter Name ASpaceMarine => " . $joe->getName() . "\n";
echo "Testing Getter HP ASpaceMarine => " . $joe->getHp() . "\n";
echo "Testing Getter AP ASpaceMarine => " . $joe->getAp() . "\n";

try
{
    echo "Trying to attack something that is not a unit: ";
    $monster->attack(3);
}
catch (Exception $e)
    {
        echo $e->getMessage() . "\n";
    }

try
{
    echo "Trying to move close to something that is not a unit: ";
    $monster->moveCloseTo(3);
}
catch (Exception $e)
    {
        echo $e->getMessage() . "\n";
    }

$testClass = new ReflectionClass("AMonster");
if ($testClass->isAbstract())
    echo "AMonster Class is abstract.\n";
else
    echo "AMonster Class isn't abstract.\n";

$testClass = new ReflectionClass("ASpaceMarine");
if ($testClass->isAbstract())
    echo "ASpaceMarineClass is abstract.\n";
else
    echo "ASpaceMarine Class isn't abstract.\n";