<?php

include_once($argv[1] . "/AWeapon.php");

class Shotgun extends AWeapon
{
    public function __construct($name, $apcost, $damage)
    {
        parent::__construct($name, $apcost, $damage);
    }
    
    public function attack()
    {
        echo "It Works !\n";
    }
}

try
{
    $winchester = new Shotgun("Shotgun", 5, 24);
    echo "Name : " . $winchester->getName() . "\n";
    echo "Apcost : " . $winchester->getApcost() . "\n";
    echo "Damage : " . $winchester->getDamage() . "\n";
    $winchester->attack();
    new Shotgun("Shotgun", null, 24);
}
catch (Exception $e)
    {
        echo "Test Exception : " . $e->getMessage() . "\n";
    }

$testClass = new ReflectionClass("AWeapon");
if ($testClass->isAbstract())
    echo "Class is abstract.\n";
else
    echo "Class isn't abstract.\n";