<?php

include_once($argv[1] . "/SpaceArena.php");
include_once($argv[1] . "/AssaultTerminator.php");
include_once($argv[1] . "/TacticalMarine.php");
include_once($argv[1] . "/RadScorpion.php");
include_once($argv[1] . "/SuperMutant.php");

$arena = new SpaceArena();

$arena->enlistMonsters([new RadScorpion(), new SuperMutant(), new RadScorpion()]);
$arena->enlistSpaceMarines([new TacticalMarine("Joe"), new AssaultTerminator("Abaddon"), new TacticalMarine("Rose")]);

$arena->fight();

$arena->enlistMonsters([new SuperMutant(), new SuperMutant()]);
$arena->fight();

try
{
    $arena->enlistMonsters([new SuperMutant(), "toto"]);
}
catch (Exception $e)
    {
        echo $e->getMessage() . "\n";
    }

try
{
    $arena->enlistSpaceMarines([new SuperMutant()]);
}
catch (Exception $e)
    {
        echo $e->getMessage() . "\n";
    }

$arena->fight();