<?php

include_once($argv[1] . "/AMonster.php");
include_once($argv[1] . "/ASpaceMarine.php");
include_once($argv[1] . "/PlasmaRifle.php");
include_once($argv[1] . "/PowerFist.php");

class Monster extends AMonster
{
    public function __construct($name)
    {
        parent::__construct($name);
        $this->ap =  50;
        $this->apcost = 10;
        $this->damage = 50;
        $this->hp = 1;
    }

    public function __destruct()
    {
    }
}

class SpaceMarine extends ASpaceMarine
{
    public function __construct($name)
    {
        parent::__construct($name);
        $this->ap = 50;
        $this->hp = 1;
    }

    public function __destruct()
    {
    }
}

$monster = new Monster("BigFoot");

$joe = new SpaceMarine("Joe");
$rifle = new PlasmaRifle();
$fist = new PowerFist();

echo "Testing Getter Name ASpaceMarine => " . $joe->getName() . "\n";
echo "Testing Getter HP ASpaceMarine => " . $joe->getHp() . "\n";
echo "Testing Getter AP ASpaceMarine => " . $joe->getAp() . "\n";

try
{
    echo "Trying to attack something that is not a unit: ";
    $joe->attack(3);
}
catch (Exception $e)
    {
        echo $e->getMessage() . "\n";
    }

try
{
    echo "Trying to move close to something that is not a unit: ";
    $joe->moveCloseTo(3);
}
catch (Exception $e)
    {
        echo $e->getMessage() . "\n";
    }

try
{
    echo "Trying to equip something that is not an AWeapon: ";
    $joe->equip(3);
}
catch (Exception $e)
    {
        echo $e->getMessage() . "\n";
    }

echo "Trying to moveCloseTo self : ";
$joe->moveCloseTo($joe);
echo "\n";
$joe->attack($monster);
$joe->equip($rifle);
$baracus = new SpaceMarine("Baracus");
echo "Re-equipping weapon : ";
$baracus->equip($rifle);
echo "\n";
$joe->attack($monster);
echo "Joe Ap : " . $joe->getAp() . "\n";
echo "BigFoot Hp after attack : " . $monster->getHp() . "\n";
$joe->recoverAp();
echo "Recover ap : " . $joe->getAp() . "\n";

echo "Move close to self : ";
$joe->moveCloseTo($joe);
echo "\n";
$joe->equip($fist);
echo "Equipping a weapon dropped, should work : ";
$baracus->equip($rifle);
echo "\n";
$joe->attack($baracus);
$joe->moveCloseTo($baracus);
$joe->attack($baracus);
echo "Baracus hp after being attacked = " . $baracus->getHp() . "\n";
$baracus->attack("toto");
$baracus->equip("toto");
$baracus->receiveDamage(-2);
$baracus->moveCloseTo("toto");
$baracus->recoverAp();
