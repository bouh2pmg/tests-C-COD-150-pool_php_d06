<?php

include_once($argv[1] . "/IUnit.php");

class Unit implements IUnit
{
    public function equip($weapon)
    {
    }

    public function attack($target)
    {
    }

    public function receiveDamage($dmg)
    {
    }

    public function moveCloseTo($target)
    {
    }

    public function recoverAP()
    {
    }
}

new Unit();
var_dump(get_class_methods("IUnit"));

$testClass = new ReflectionClass("IUnit");
if ($testClass->isInterface())
    echo "Class is an interface.\n";
else
    echo "Class isn't an interface.\n";