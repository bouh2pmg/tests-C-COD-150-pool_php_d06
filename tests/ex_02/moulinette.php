<?php

include_once($argv[1] . "/PlasmaRifle.php");
include_once($argv[1] . "/PowerFist.php");

$rifle = new PlasmaRifle();
$fist = new PowerFist();

echo "PlasmaRifle Name : " . $rifle->getName() . "\n";
echo "PlasmaRifle Apcost : " . $rifle->getApcost() . "\n";
echo "PlasmaRifle Damage : " . $rifle->getDamage() . "\n";
if ($rifle->isMelee() === false)
    echo "PlasmaRifle is not melee." . "\n";
else
    echo "PlasmaRifle shouldn't be a melee weapon.\n";
echo "PlasmaRifle attack : ";
$rifle->attack();

echo "PowerFist Name : " . $fist->getName() . "\n";
echo "PowerFist Apcost : " . $fist->getApcost() . "\n";
echo "PowerFist Damage : " . $fist->getDamage() . "\n";
if ($fist->isMelee() === true)
    echo "PowerFist is melee.\n";
else
    echo "PowerFist should be a melee weapon.\n";
echo "PowerFist attack : ";
$fist->attack();
